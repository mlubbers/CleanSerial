module listDevices

import CleanSerial
import Text
import Data.Tuple

Start w = appFst (join "\n") (getTTYDevices w)
