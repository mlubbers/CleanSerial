CFLAGS?=-Wall -Wextra

ifeq ($(OS), Windows_NT)
DETECTED_OS?=Windows
CC=gcc
PLATFORMDIR=src-windows
else
DETECTED_OS?=POSIX
PLATFORMDIR=src-posix
endif

LIBFILES=$(wildcard $(PLATFORMDIR)/CleanSerial/*library)

VPATH = src:./csource

all: $(addprefix src/Clean\ System\ Files/,ctty.o $(LIBFILES))

src/Clean\ System\ Files/%: %
	mkdir -p "src/Clean System Files"
	cp "$<" "src/Clean System Files"

ctty.o: tty.c
	$(COMPILE.c) $(OUTPUT_OPTION) $<

clean:
	$(RM) -rf *.o $(addsuffix /Clean\ System\ Files,src $(PLATFORMDIR)) CleanSerial

pkg: all
	mkdir CleanSerial
	cp -R src/* CleanSerial
	cp -R $(PLATFORMDIR)/* CleanSerial
