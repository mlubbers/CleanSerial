implementation module CleanSerial.iTasks

import Data.Tuple
import StdEnv

from Data.Map import :: Map, newMap
import Data.Func
import Text

import iTasks
import iTasks.Internal.IWorld
import iTasks.Internal.SDS
import iTasks.Internal.Util
import iTasks.Internal.Task
import iTasks.Internal.TaskEval
import iTasks.Internal.TaskState

import CleanSerial

:: *Resource | TTYd String *TTY

derive class iTask TTYSettings, Parity, BaudRate, ByteSize

syncSerialChannel :: Timespec TTYSettings (b -> String) (String -> (Either String [a], String)) (Shared sds ([a],[b],Bool)) -> Task () | iTask a & iTask b & RWShared sds
syncSerialChannel poll opts enc dec rw = Task evalinit
where
	evalinit DestroyEvent _ iworld = (DestroyedResult, iworld)
	evalinit event evalOpts iworld
		# (mtty, iworld=:{world,resources}) = getResource iworld
		= case mtty of
			[] = case TTYopen opts world of
				(False, _, world)
					= appFst exc $ liftIWorld TTYerror {iworld&world=world}
				(True, tty, world)
					= eval "" event evalOpts {iworld&world=world,resources=[TTYd opts.devicePath tty:resources]}
			_ = (exc "This tty was already open", iworld)

	withTTY f iworld
		# (mtty, iworld) = getResource iworld
		= case mtty of
			[] = (exc "This tty was already closed", iworld)
			[_,_:_]  = (exc "Multiple matching resources", iworld)
			[tty] = f tty iworld

	eval acc DestroyEvent evalOpts
		= withTTY \(TTYd _ tty) iworld
			# (ok, iworld) = liftIWorld (TTYclose tty) iworld
			| not ok = (exc "Couldn't close device", iworld)
			= (DestroyedResult, iworld)

	eval acc event evalOpts=:{taskId,lastEval}
		= withTTY \(TTYd dp tty) iworld
			# (merr, iworld) = readRegister taskId ticker iworld
			| isError merr = (ExceptionResult (fromError merr), iworld)
			//TODO Keep async in mind
			# (merr, iworld) = read rw EmptyContext iworld
			| isError merr = (ExceptionResult (fromError merr), iworld)
			= case fromOk merr of
				//We need to stop
				ReadingDone (_,_,True)
						# (ok, iworld) = liftIWorld (TTYclose tty) iworld
						| not ok = (exc "Couldn't close device", iworld)
						= (ValueResult NoValue (mkTaskEvalInfo lastEval) NoChange (return ()), iworld)
				ReadingDone (r,s,ss)
					# tty = foldr TTYwrite tty $ reverse $ map enc s
					# (merr, tty) = readWhileAvailable tty
					| isError merr = (exc (fromError merr), iworld)
					# iworld = {iworld & resources=[TTYd dp tty:iworld.resources]}
					= case dec (acc +++ toString (fromOk merr)) of
						(Left err, newacc) = (exc ("Error while parsing: " +++ join " " [toString (toInt c)\\c<-:acc+toString (fromOk merr)]), iworld)
						(Right msgs, newacc)
							# (merr, iworld) = if (msgs =: [] && s =: [])
								(Ok WritingDone, iworld)
								//TODO Keep async in mind
								(write (r++msgs, [], False) rw EmptyContext iworld)
							| isError merr = (ExceptionResult (fromError merr), iworld)
							= (ValueResult
								NoValue
								(mkTaskEvalInfo lastEval)
								(mkUIIfReset event $ stringDisplay $ "Serial client" <+++ opts.devicePath)
								(Task (eval newacc))
							  , iworld)

	ticker = sdsFocus {start=zero,interval=poll} iworldTimespec

	getResource = iworldResource (\t=:(TTYd p _)->(p == opts.devicePath, t))

	exc :: (String -> TaskResult ())
	exc = ExceptionResult o exception

readWhileAvailable :: !*TTY -> (MaybeError String [Char], !*TTY)
readWhileAvailable tty
# (available, error, tty) = TTYavailable tty
| error = (Error "TTY device disconnected", tty)
| not available = (Ok [], tty)
# (c, tty) = TTYread tty
# (merr, tty) = readWhileAvailable tty
| isError merr = (merr, tty)
= (Ok [toChar c:fromOk merr], tty)
