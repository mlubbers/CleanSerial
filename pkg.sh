#!/bin/sh
set -e
# 64 bit linux
make clean pkg
tar czf CleanSerial-linux-x64.tar.gz CleanSerial

# 32 bit linux
make CFLAGS=-m32 clean pkg
tar czf CleanSerial-linux-x86.tar.gz CleanSerial
make clean

# 64 bit windows
make DETECTED_OS=Windows CC=x86_64-w64-mingw32-gcc-win32 CFLAGS=-m64 PLATFORMDIR=src-windows pkg
zip -rq CleanSerial-windows-x64.zip CleanSerial

# 32 bit windows
make DETECTED_OS=Windows CC=x86_64-w64-mingw32-gcc-win32 CFLAGS=-m32 PLATFORMDIR=src-windows clean pkg
zip -rq CleanSerial-windows-x86.zip CleanSerial
