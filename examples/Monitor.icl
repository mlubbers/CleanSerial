module Monitor

import StdEnv

import Data.Either
import Data.Func
import System.Time

import iTasks

import CleanSerial, CleanSerial.iTasks

Start w = flip doTasks w $
	parallel [] [OnAction (Action "+") $ always (Embedded, addDevice)]
	<<@ ArrangeWithTabs True
	>>* [OnAction (Action "Shutdown") $ always (shutDown 0)]

addDevice :: (SharedTaskList ()) -> Task ()
addDevice stl = tune (Title "New device") $
	           accWorld getTTYDevices
	>>= \ds->  enterChoice "Choose path" [] ["-- Other --":ds]
	>>= \path->updateInformation "TTY Settings" [] {zero & devicePath=path}
	>>! \ts->  appendTask Embedded (\_->monitor ts) stl
	@! ()

monitor :: TTYSettings -> Task ()
monitor ts = tune (Title ts.devicePath) $
	catchAll (
		withShared ([], [], False) \channels->
		    syncSerialChannel
				{tv_sec=0,tv_nsec=100*1000000}          //poll interval
				ts                                      //tty settings
				id                                      //encode
				(\s->(Right (if (s == "") [] [s]), "")) //decode
				channels                                //channels
		||- viewSharedInformation "Incoming messages" [ViewAs (take 20 o fst3)] channels
		||- forever (
			enterInformation "Send line of text" []
			>>= \line->upd (\(r,w,s)->(r,w++[line+++"\n"],s)) channels
		) @? const NoValue
	) (\e->viewInformation "Exception occured" [] e)
	>>* [OnAction (Action "Close") $ always (treturn ())]
